<?php


require_once('path-to-fpdf/fpdf.php');

//Setup FPDF

//Set the output filename
$filename="output.pdf";

class PDF extends FPDF
{
function Header()
{
    $this->SetFont('Arial','BU',15);
    $this->Cell(80);
    $this->Cell(30,10,'Renter Application',0,0,'C');
    $this->Ln(12);
}
}

//Create PDF
$pdf = new PDF();
$pdf->AddPage();

foreach ($_POST as $key =>$data)
{
  $pdf->SetFont('Arial','BU',15);
  switch ($key){
    case "a-Name":
      $i = 3;
      $pdf->Cell(30,10,"Applicant's Information",0,1,'L');
      break;
    case "Employer":
      $i = 3;
      $pdf->Ln(10);
      $pdf->Cell(30,10,"Applicant's Employment Information",0,1,'L');
      break;
    case "s-question":
      $i = 3;
      $pdf->SetFont('Arial','',12);
      $pdf->Ln(10);
      $pdf->write(6,"Do you have a spouse or a significant other that will be living with you?");
      $pdf->Write(6, " $data ");
      continue 2;
    case "o-question":
      $i = 3;
      $pdf->SetFont('Arial','',12);
      $pdf->Ln(10);
      $pdf->write(6,"Do you have another occupant such as a child or relative that will be living with you?");
      $pdf->Write(6, " $data ");
      continue 2;
    case "s-Name":
      $i = 3;
      $pdf->Ln(10);
      $pdf->Cell(30,10,"Spouse or Significant Other's Information",0,1,'L');
      break;
    case "s-Employer":
      $i = 3;
      $pdf->Ln(10);
      $pdf->Cell(30,10,"Spouse or Significant Other's Employment",0,1,'L');
      break;
    case "o-Name":
      $i = 3;
      $pdf->Ln(10);
      $pdf->Cell(30,10,"Other Occupants",0,1,'L');
      break;
    case "Business_or_Landlord's_Name":
      $i = 3;
      $pdf->Ln(10);
      $pdf->Cell(30,10,"Rental History",0,1,'L');
      break;
    case "Contact's_Name":
      $pdf->AddPage();
      $i = 3;
      $pdf->Ln(6);
      $pdf->Cell(30,10,"Emergency Contact Information",0,1,'L');
      break;
    case "Applicant's_Initial":
      $i = 3;
      $pdf->Ln(6);
      $pdf->Cell(30,10,"Application Authorization",0,1,'L');
      $pdf->SetFont('Arial','B',12);
      $pdf->Write(6, "I Authorize, insert agreement statement here.");
      $pdf->Ln(6);
      break;
  }
    //Remove commas from modkey strings
    $pdf->SetFont('Arial','',12);
    $search = array("s-", "o-", "a-", "_");
    $replace = array("", "", "", " ");
    $modkey = str_replace($search, $replace, $key);

    if($i >= 1){
      $pdf->Write(6, "$modkey: $data ");
      $i--;
    } else {
      $pdf->Ln(6);
      $pdf->Write(6, "$modkey: $data ");
      $i = 2;
    }
}
//Output File
$pdf->Output('F', $filename);


echo "Thank you for submitting your application. " . "You will be redirected shortly.";
header('Refresh: 1; /index.html');
?>
